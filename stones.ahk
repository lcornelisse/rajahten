class Stone {

    static Stones := {}
    control_id := 0
    pos := new Dimensions(0, 0, 25, 25)

    redraw_on_pos := false

    __new(magic) {
        this.magic := magic
        this.alt_magic := 0
    }

    create_button(inscription) {
        
        this.redraw_on_pos := false

        Opt1 := [3, 0xEEF1F4, 0xE4E9EF, "Black", 0,, 0xA5AFBF, 1]
        Opt2 := [3, 0xCDD6DF, 0xCDD6DF, "Black", 0,, 0xA5AFBF, 1]
        Opt3 := [3, 0xBEC7D6, 0xBEC7D6, "Black", 0,, 0xA5AFBF, 1]
        Opt4 := [3, 0xBEC7D6, 0xBEC7D6, "Black", 0,, 0xA5AFBF, 1]

        Gui, Add, Button, HwndButtonId g___StoneClicked, % inscription
            ;ImageButton.Create(ButtonId, Opt1, Opt2, Opt3, Opt4)

        this.control_id := ButtonId
        Stone.Stones[ButtonId] := this

        return

        ___StoneClicked:
            GuiControlGet, focused_control, Focus
            GuiControlGet, focused_controlHwnd, Hwnd, %focused_control%

            stone := Stone.stones[focused_controlHwnd+0]
            stone.magic.call()
            return
    }

    create_image(image, w, h) {
        pos := this.pos
        pos.w := w
        pos.h := h

        this.redraw_on_pos := true

        rune := this.magic.rune

        Gui, Add, Picture, % "HwndPictureId g___StoneImageClicked x" . pos.x . " y" . pos.y . " w" . pos.w . " h" . pos.h, % image
        this.control_id := PictureId
        Stone.stones[rune.name, image] := this
        ;RajAhten().days.d("##### drag stone PictureId", PictureId, rune.name, image)

        return

        ___StoneImageClicked:
            rune_name := a_gui           
            image := a_guicontrol 
            
            ClickedTime := A_TickCount

            CoordMode, Mouse  ; Switch to screen/absolute coordinates.
            MouseGetPos, EWD_MouseStartX, EWD_MouseStartY, EWD_PictureId
            
            stone := Stone.stones[rune_name, image]

            ;RajAhten().days.d("##### drag stone clicked", EWD_PictureId, a_gui, stone.magic.rune.name)            
            SetTimer, ___StoneImageWatchMouse, 100 ; Track the mouse as the user drags it.
            return

        ___StoneImageWatchMouse:
            stone := Stone.stones[rune_name, image]

            

            ;RajAhten().days.d("##### drag stone ___StoneImageWatchMouse", rune_name, image, stone.magic.rune.name)
            GetKeyState, EWD_LButtonState, LButton, P
            if EWD_LButtonState = U  ; Button has been released, so drag is complete.
            {
                SetTimer, ___StoneImageWatchMouse, off
                ;EarthWarden().days.i("___StoneImageWatchMouse", a_tickcount, ClickedTime, (a_tickcount - ClickedTime), stone.magic.derp)
                if ((a_tickcount - ClickedTime) < 600) {
                    stone.magic.click()
                }
                return
            }
            GetKeyState, EWD_EscapeState, Escape, P
            if EWD_EscapeState = D  ; Escape has been pressed, so drag is cancelled.
            {
                SetTimer, ___StoneImageWatchMouse, off
                ;WinMove, ahk_id %EWD_MouseWin%,, %EWD_OriginalPosX%, %EWD_OriginalPosY%
                return
            }
            ; Otherwise, reposition the window to match the change in mouse coordinates
            ; caused by the user having dragged the mouse:
            CoordMode, Mouse
            MouseGetPos, EWD_MouseX, EWD_MouseY
            SetWinDelay, -1   ; Makes the below move faster/smoother.

            stone.magic.drag(EWD_MouseX - EWD_MouseStartX, EWD_MouseY - EWD_MouseStartY)
            
            EWD_MouseStartX := EWD_MouseX  ; Update for the next timer-call to this subroutine.
            EWD_MouseStartY := EWD_MouseY
            return
    }

    position(pos := "") {
        if (pos != "") { 
            this.pos := pos
        } else {
            pos := this.pos
        }
        control_id := this.control_id
        x := pos.x
        y := pos.y
        w := pos.w
        h := pos.h
        
        if (this.redraw_on_pos) {
            op = MoveDraw
        } else {
            op = Move
        }
        GuiControl, %op%, %control_id%, x%x% y%y% w%w% h%h%
    }

    get_dimensions() {
        control_id := this.control_id
        GuiControlGet, P, Pos, %control_id%

        pos := new Dimensions(PX, PY, PW, PH)

        return pos
    }

    update(inscription, magic) {
        this.magic := magic
        control_id := this.control_id
        GuiControl, , %control_id%, % inscription
        
        this.pos.h := Stone.fixed_height
        this.pos.w := StrLen(inscription) * 8
        ;RajAhten().days.i("update", inscription, this.magic.human.short)

        GuiControl, Enable, %control_id%
        GuiControl, Show, %control_id%
    }

    destroy() {
        control_id := this.control_id
        GuiControl, MoveDraw, %control_id%, x0 y0 w0 h0
        GuiControl, Disable, %control_id%
        GuiControl, Hide, %control_id%
        ;RajAhten().days.d("destroy", 1, "Hide control", control_id, this.magic.human.short)
        Stone.Stones.delete(control_id)
        this.magic := ""
        ;RajAhten().days.d("destroy 2", control_id)
    }

    clear() {
        control_id := this.control_id
        ;GuiControl, MoveDraw, %control_id%, x0 y0 w0 h0
        GuiControl, Disable, %control_id%
        GuiControl, Hide, %control_id%
        ;RajAhten().days.d("clear", 1, "Hide control", control_id, this.magic.human.short)
        ;Stone.Stones.delete(control_id)
        this.magic := ""
        ;RajAhten().days.d("clear 2", control_id)
    }

    string() {
        return this.control_id
    }
}

class StoneSet {

    stones := new Circle()
    magics := new Circle()
    
    inactive := new Circle()

    is_destroyed := false

    __new(rune) {
        ;days := RajAhten().days
        ;days.d("new StoneSet(" . rune.name . ")")

        this.rune := rune
    }

    __delete() {
        this.destroy()
    }

    destroy() {
        if (this.is_destroyed = false) {
            this.is_destroyed := true
            this.rune := ""
            
            this.stones := ""
        }
    }

    string() {
        s := "StoneSet ["
        comma := ""
        for i, stone in this.stones.elements() {
            s := s . comma . stone.string()
            comma := ", "
        } 
        s := s . "]"
        return s
    }

    clear() {
        while (this.stones.length() > 0) {
            this.replace_a_stone()
        }

        while (this.magics.length() > 0) {
            this.magics.pop()
        }

    }

    create_stones(humans) {
        ;days := RajAhten().days
        ;days.i("create_stones", humans.length(), "===============")

        this.rune.set_default_gui()

        for i, human in humans {
            if (human.name != this.rune.name) {
                ;days.d("create_stones", i, human.name)
                this.insert_human(human)
            }
        }

        ;days.d("create_stones", "done",  "===============")
        this.position_stones()
    }

    reinscribe_stones() {
        this.rune.set_default_gui()

        ;days := RajAhten().days
        ;days.d("reinscribe_stones", "======== ################################## ", this.magics.length(), this.stones.length())

        dead := []
        for i, magic in this.magics.elements() {
            magic.human.update()
            if (magic.human.is_dead) {
                dead.push(human)
            }
        }

        for i, human in dead {
            this.remove_human(human)
        }

        magics := this.magics.elements()

        for i, stone in this.stones.elements() {
            magic := magics[i]
            ;days.d("reinscribe_stones", "update", magic.human.short)
            ;magic.human.update()
            
            stone.update(magic.human.short, magic)
        }        
    }

    position_stones() {
        this.reinscribe_stones()
        this.rune.set_default_gui()

        x := 55
        y := 0

        ;days := RajAhten().days
        ;days.d("position_stones", x, y)

        for i, stone in this.stones.elements() {
            pos := stone.get_dimensions()
            pos.x := x
            pos.y := 0
            pos.h := RuneOfControl.fixed_height
            pos.w := StrLen(stone.magic.human.short) * 10
            
            stone.position(pos)
            ;days.d("position_stones", this.rune.name, human_name, pos.string())
            x += pos.w + 2
        }        
    }

    insert_human(human) {

        this.rune.set_default_gui()
        ;days := RajAhten().days
        ;days.d("insert_human", this.rune.name, 1, human.short)
        
        magic := new HumanEarthMagic(this.rune, human)
        this.magics.push(magic)
        stone := this.grab_a_stone(human.short, magic) 
        
    }

    has_human(human) {
        for i, magic in this.magics.elements() {
            if (magic.human.name = human.name) {
                return true
            }
        }
        return false
    }

    remove_human(human) {
        ;days := RajAhten().days
        ;days.d("remove_human", this.rune.name, human.short)
        
        this.rune.set_default_gui()
        
        this.replace_a_stone()

        x := false
        for i, magic in this.magics.elements() {
            if (magic.human.name = human.name) {
                ;days.d("remove_human", "found magic", human.name)
                x := magic
                break
            }
        }

        if (x) {
            this.magics.remove(magic)
        }

        ;stone := this.stones.
        ;stone := this.stones[human.name]
        ;this.stones.delete(human.name)
        ;days.d("remove_human", 2, stone.string())
        ;stone.spend()
        ;magics.remove(stone.magic)
   
    }

    grab_a_stone(inscription, magic) {
        
        ;RajAhten().days.d("grab_a_stone", inscription)

        if (this.inactive.length() > 0) {
            ;RajAhten().days.d("grab_a_stone", "reusing inactive stone")
            stone := this.inactive.pop()
            stone.update(inscription, magic)
        } else {
            ;RajAhten().days.d("grab_a_stone", "creating new stone")            
            stone := new Stone(magic)
            stone.create_button(inscription)
        }
        
        ;RajAhten().days.d("grab_a_stone", "push stone")
        this.stones.push(stone)

        return stone

    }

    replace_a_stone() {
        stone := this.stones.pop()
        stone.clear()
        this.inactive.push(stone)
    }

}