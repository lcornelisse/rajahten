;
; The House Of Understanding
;

; 
; The Room of Shapes
;
class Dimensions {
    X := 0
    Y := 0
    W := 0
    H := 0

    __new(x, y, w, h) {
        this.X := x
        this.Y := y
        this.W := w
        this.H := h
    }

    string() {
        return "pos (" . this.X . ", " . this.y . ") " . this.w . "x" . this.h
    }

    clone() {
        return new Dimensions(this.x, this.y, this.w, this.h)
    }

    eq(that) {
        if (that) {
            if (this.x != that.x) 
                return false
            if (this.y != that.y) 
                return false
            if (this.w != that.w) 
                return false
            if (this.h != that.h) 
                return false
            return true
        }
        return false
    }

    neq(that) {
        
        if (that) {
            if (this.x != that.x) {
                ;EarthKing().days.d("neq", "true", "x", this.x, that.x)
                return true
            }

            if (this.y != that.y) {
                ;EarthKing().days.d("neq", "true", "y", this.y, that.y)
                return true
            }
            
            if (this.w != that.w) {
                ;EarthKing().days.d("neq", "true", "w", this.w, that.w)
                return true
            }
            
            if (this.h != that.h) {
                ;EarthKing().days.d("neq", "true", "h", this.h, that.h)
                return true
            }
            ;EarthKing().days.d("neq", "false", "x", this.x, that.x, "y", this.y, that.y, "w", this.w, that.w, "h", this.h, that.h)        
            return false
        }        
        ;EarthKing().days.d("neq", "true", "that", "=^.^=")
        return true    
    }
    
}

class Circle {

    arr := []
    buf := []
    token := 1
    destroyed := false

    merges := 0
    merge_copies := 0
    gnomes := 0
    merge_depth := 0
    merge_depth_max := 0

    __new() {
    }

    __delete() {
        if (this.destroyed = false) {
            this.destroy()
        }
    }

    wrap(arr) {
        this.arr := arr
    }

    destroy() {
        if (this.destroyed = false) {
            this.arr := ""
            this.destroyed := true
        }
    }

    eq(that, key) {

        if (this.length() != that.length()) {
            ;Gaborn().days.i("Circle.eq", "this.length() != that.length()", this.length(), that.length())
            return false
        }

        for i, x in that.elements() {
            if (x[key] != this.arr[i][key]) {
                ;Gaborn().days.i("Circle.eq", "x[key] != this.arr[i][key]", x[key], this.arr[i][key])
                return false
            }
            ;Gaborn().days.i("Circle.eq", "x[key] == this.arr[i][key]", x[key], this.arr[i][key])
        }
        ;Gaborn().days.i("this == that")
        return true
    }

    elements() {
        return this.arr
    }

    length() {
        return this.arr.length()
    }

    at_front() {
        return this.arr[1]
    }

    at_back() {
        return this.arr[this.arr.length()]
    }

    at_token() {
        return this.arr[this.token]
    }

    at(i) {
        return this.arr[i]
    }

    insert_at_front(x) {
        this.arr.insertAt(1, x)
    }

    insert(x) {
        this.arr.push(x)
    }

    remove(x) {
        loop, % this.arr.length() 
        {
            if (this.arr[a_index] = x) {
                this.arr.removeAt(a_index)
                return true                
            }
        }
        return false
    }

    remove_by_key(x, key) {
        loop, % this.arr.length() 
        {
            if (this.arr[a_index][key] = x[key]) {
                this.arr.removeAt(a_index)
                return true
            }
        }
        return false
    }

    remove_by_key_true(key) {
        loop, % this.arr.length() 
        {
            if (this.arr[a_index][key]) {
                this.arr.removeAt(a_index)
                return true
            }
        }
        return false        
    }

    remove_by_key_equals(x, key) {
        loop, % this.arr.length() 
        {
            if (this.arr[a_index][key] = x) {
                this.arr.removeAt(a_index)
                return true
            }
        }
        return false        
    }

    has(x) {
        loop, % this.arr.length() 
        {
            if (this.arr[a_index] = x) {
                return true
            }
        }
        return false
    }

    has_by_key(x, key) {
        ;Gaborn().days.d("has_by_key", this.arr.length())
        loop, % this.arr.length() 
        {
            ;Gaborn().days.d("has_by_key", this.arr[a_index].string(), this.arr[a_index][key], x[key])
            if (this.arr[a_index][key] = x[key]) {
                return true
            }
        }
        return false
    }

    has_by_key_equals(x, key) {
        ;Gaborn().days.d("has_by_key", this.arr.length())
        loop, % this.arr.length() 
        {
            ;Gaborn().days.d("has_by_key", this.arr[a_index].string(), this.arr[a_index][key], x[key])
            if (this.arr[a_index][key] = x) {
                return true
            }
        }
        return false
    }


    push(x) {
        ;Gaborn().days.d("circle push", x.name)
        this.arr.push(x)
    }

    pop() {
        return this.arr.pop()
    }

    whorl() {
        ;RajAhten().days.d("whorl")
        
        x := this.arr.pop()
        this.arr.insertAt(1, x)
    }

    anti_whorl() {
        x := this.arr.removeAt(1)
        this.arr.push(x)
    }

    whorl_about(locus) {
        ;RajAhten().days.d("whorl_about", locus)
        if (locus > 0) {
            x := this.arr.removeAt(locus)
            this.whorl()
            this.arr.insertAt(locus, x)
        } else {
            this.whorl()
        }
    }

    anti_whorl_about(locus) {
        if (locus > 0) {
            x := this.arr.removeAt(locus)
            this.anti_whorl()
            this.arr.insertAt(locus, x)
        } else {
            this.anti_whorl()
        }
    }

    ; endowment from nist dictionary of data structures and algortihms
    gnomesort(key) {
        this.gnomesort_range(key, 1, this.arr.length() + 1)
    } 

    gnomesort_range(key, p, q) {
        
;
        this.gnomes += 1

        arr := this.arr
        i := p
        tmp := 0
        while (i < q) {
            if (i = p or arr[i-1][key] <= arr[i][key]) {      
                ; Outer loop functionality --- scan to larger index  
                i++                               
            } else {                               
                ; Inner loop functionality --- move to smaller index 
                tmp := arr[i]
                arr[i] := arr[i-1]
                arr[--i] := tmp      
            }                                      
        }                                          
    } 

    ; the Some call me... Tim? Sort.
    ; a merge sort, but when the list is small, use gnome sort.
    sort_range(key, p, q) {
        arr := this.arr
        n := q - p

        if (n < 10) {
            this.gnomesort_range(key, p, q)
        } else {

            r := p + floor(n / 2)
            this.sort_range(key, p, r)
            this.sort_range(key, r, q)
            this.merge_ranges(key, p, r, q)

        }             
    }

    merge_deeper() {
        md := this.merge_depth += 1
        mdm := this.merge_depth_max
        
;
        this.merge_depth := md
        
        this.merge_depth_max := md > mdm ? md : mdm
    }

    merge_deepless() {
        this.merge_depth -= 1
    }


    merge_ranges(key, p, r, q) {
        arr := this.arr
        i := p
        j := r
        tmp := 0

;
        this.merges += 1

        buf := this.buf
    
        ; While there are elements in the left or right runs...
        k := p
        while (k < q) {
            ; If left run head exists and is <= existing right run head.
            if (i < r and (j >= q or arr[i][key] <= arr[j][key])) {
                buf[k] := arr[i]
                i += 1
            } else {
                buf[k] := arr[j]
                j += 1    
            }
            k++
        } 

        k := p
        while (k < q) {
            arr[k] := buf[k]
            k++
        }

    }

    sort(key) {

        this.merges := 0
        this.merge_copies := 0
        this.gnomes := 0
        this.merge_depth := 0
        this.merge_depth_max := 0

        arr := this.arr
        n := arr.length() + 1
        i := 1

        this.sort_range(key, i, n)                               
        this.buf := []
    } 
}

;
; The Room of Creatures
;
Human_ActiveTimeSortingHat(a, b) {
    ;Gaborn().days.i("Human_ActiveTimeSortingHat", a.name, a.last_active, b.name, b.last_active)
    return a.last_active - b.last_active
}

class SeedOfHumanity {

    static last_adjustment_by_class := {}

    static stubborn_classes := ["Corel", "Xming"]

    pos := new Dimensions(0,0,0,0)
    actual_pos := new Dimensions(0,0,0,0)
    offs_x := 0
    offs_y := 0

    rune := ""
    clothes := false

    in_danger := false
    feeling_scared := false
    is_attacking := false
    is_active := false
    is_minimized := false
    is_maximized := false
    is_normal := false

    minmax_state := false
    minmax_state_changed := false

    adjusted := false

    caption_ww := new WaterWizard(false, true)

    ; this is a timestamp that updates when the window is active
    last_active := 0

    __new(name) {

        ;Gaborn().days.d("********************* NEW HUMAN", name)
        this.name := name
    
        WinGetClass, x, ahk_id %name%
        this.class := x

        WinGet, exe, ProcessName, ahk_id %name%
        this.exe := exe

        ; have we seen this kind of folk around here before?
        ;adj := Human.last_adjustment_by_class[x]

        ;if (adj) {
        ;    this.adjustment := adj.clone()
        ;} else {
            this.adjustment := new Dimensions(0,0,0,0)
        ;}

        this.stubborn := false

        for i, regex in Human.stubborn_classes {
            found := regexmatch(x, regex)
            ;Gaborn().days.d("stubborn?", this.class, x, regex, "stubborn", found)
            if (found)
                this.stubborn := true
        }
        
        this.update()
        this.update_minmax()
        this.minmax_state_changed := false
        
        ;Gaborn().days.d("new human", this.short, this.class, this.title, "stubborn", this.stubborn)
        
    }

    update() {
        name := this.name
        if (WinExist("ahk_id " . name)) {
            this.is_dead := false    

            WinGetTitle, x, ahk_id %name%
            this.title := x

            arr := StrSplit(x, "-")
            s := arr[1]
            this.short := SubStr(s, 1, 40)
            if (strlen(this.short) >= 40) {
                this.short := this.short . "..."
            }

            ;Gaborn().days.d("update human", this.short, this.class, this.title)
            prp := WinGetPosEx(name, x, y, w, h, offs_x, offs_y)

            if (prp) {
                pos := this.pos
                actual_pos := this.actual_pos
                actual_pos.x := x
                actual_pos.y := y
                actual_pos.w := w
                actual_pos.h := h
                this.offs_x := offs_x
                this.offs_y := offs_y

                adjustment := this.adjustment
                adjustment.x := offs_x
                adjustment.y := offs_y
                ;adjustment.w := Abs(pos.w - actual_pos.w) + 1
                ;adjustment.h := Abs(actual_pos.h - pos.h) + 1

                adjustment.w := offs_x * -2
                adjustment.h := offs_y * -2
                
                if ((pos.x + adjustment.x) < 0) {
                    ; when we go too far off the edges of the screen, the window 
                    ; may become unresponsive
                    adjustment.w -= 4
                    adjustment.x += 2
                }

                if (this.clothes) {
                    resize_by := this.clothes.resize_by
                    s := resize_by.string()
                        ;msgbox here, %s%
                    
                    if (resize_by) {
                        adjustment.x += resize_by.x
                        adjustment.y += resize_by.y
                        adjustment.w += resize_by.w
                        adjustment.h += resize_by.h
                    }
                }

            } else {
                msgbox wtf
            }

            IfWinActive ahk_id %name%
            { 
                this.is_active := true
                this.last_active := A_TickCount
                EarthKing().land.active := this
            } else {
                this.is_active := false
            }    

            if (this.rune.is_destroyed) {
                this.rune := ""
            }  
            
        } else {
            this.is_dead := true
            if (this.rune) {
                this.rune.human := false
            }
        }  

    }

    update_minmax() {
        name := this.name
        WinGet, state, MinMax, ahk_id %name%
            
        last_minmax_state := this.minmax_state
        this.minmax_state := state
 
        this.minmax_state_changed := ((last_minmax_state != state) ? true : false) 

        this.is_minimized := ((state < 0) ? true : false)
        this.is_maximized := ((state > 0) ? true : false)
        this.is_normal    := ((state = 0) ? true : false)

        ;if (this.minmax_state_changed) {
        ;    Gaborn().days.d("human.update", this.short, "mixmax_state_changed", (state > 0) ? "max" : (state < 0) ? "min" : "normal", state)
        ;}

    }
    
    string() {
        return % "<<" . this.exe . ", " . this.title . ", " . this.class . ", " . this.name . ">>"
    }

    dump() {
        s := this.string() . "`n"
        s := s . "              pos: " . this.pos.string() . "`n"
        s := s . "       actual_pos: " . this.actual_pos.string() . "`n"
        s := s . "           offs_x: " . this.offs_x . "`n"
        s := s . "           offs_y: " . this.offs_y . "`n"
        s := s . "             rune: " . this.rune.name . "`n"
        s := s . "          clothes: " . this.clothes.string() . "`n"
        s := s . "        in_danger: " . this.in_danger . "`n"
        s := s . "   feeling_scared: " . this.feeling_scared . "`n"
        s := s . "     is_attacking: " . this.is_attacking . "`n"
        s := s . "        is_active: " . this.is_active . "`n"
        s := s . "     is_minimized: " . this.is_minimized . "`n"
        s := s . "     is_maximized: " . this.is_maximized . "`n"
        s := s . "        is_normal: " . this.is_normal . "`n"
         
        return s
    }

    hide() {
        this.is_attacking := false      
        name := this.name
        this.feeling_scared := false
        WinMinimize, ahk_id %name%  
    }
    
    strike() {
        this.is_attacking := true
        if (!this.feeling_scared) {
            if (this.rune) {
                this.attack_rune(this.rune)
                return
            } 
        }
        this.hide() 
    }

    toggle_caption() {
        if (this.caption_ww.poke()) {
            this.show_caption()
        } else {
            this.hide_caption()
        }
    }

    hide_caption() {
        name := this.name
        WinSet, Style, -0xc00000, ahk_id %name%
    }

    show_caption() {
        name := this.name
        WinSet, Style, +0xc00000, ahk_id %name%
    }

    maximize() {
        name := this.name
        WinMaximize ahk_id %name%
    }

    attack_rune(rune) {

        this.rune := rune
        rune.human := this

        next_pos := rune.get_adjusted_human_pos()

        if (rune.hidden) {
            next_pos := rune.human_pos_rune_hidden.clone()
        } else {
            next_pos := rune.human_pos.clone()
        }

        if (this.pos.neq(next_pos) or this.minmax_state_changed) {
            ;EarthKing().days.d("attack_rune", human.short, rune.name, "minmax changed:", this.minmax_state_changed) 
            this.pos := next_pos
            this.position()
            this.adjust_size()
        }

        rune.reinscribe_if_needed()

    }

    attention() {
        this.stubborn_start()    
        
        name := this.name
        WinActivate, ahk_id %name%
        WinWaitActive, ahk_id %name%

        this.stubborn_stop()    
    }

    position() {
        name := this.name

        ;Gaborn().days.i("human pos", this.short, pos.x, pos.y, pos.w, pos.h)
        this.update()
        
        if (this.stubborn) {
            this.aggressive_position()
            return
        }
     
        if (this.is_minimized) {
            WinRestore, ahk_id %name%
        }
     
        pos := this.pos.clone()
        adj := this.adjustment
        
        pos.x += adj.x
        pos.y += adj.y
        pos.w += adj.w
        pos.h += adj.h

        ;if (!this.is_maximized) {
           WinMove, ahk_id %name%, , pos.x, pos.y, pos.w, pos.h
        ;}   
    
    }

    adjust_size() {

        ;if (this.adjusted) {
        ;    return
        ;}

        this.update()

        pos := this.pos
        actual_pos := this.actual_pos

        adjust := false

        if (pos.x != actual_pos.x) {
            adjust := true        

        } else if (pos.y != actual_pos.y) {
            adjust := true        

        } else if (pos.w != actual_pos.w) {
            adjust := true        

        } else if (pos.h != actual_pos.h) {
            adjust := true        
        }

        if (adjust) {
            this.position()
        }
    }

    stubborn_start() {
        if (this.stubborn) {
            ;SetTitleMatchMode, 2
            ;SetTitleMatchMode, Slow
            SetWinDelay, 100   
            ;WinSet, AlwaysOnTop, On, ahk_id %name%
        }
    }

    stubborn_stop() {
        if (this.stubborn) {
            SetWinDelay, -1
            ;SetTitleMatchMode, 1            
            ;SetTitleMatchMode, Fast
            ;WinSet, AlwaysOnTop, Off, ahk_id %name%
        }
    }
    
    aggressive_position() {
        this.stubborn_start()
        name := this.name

        pos := this.pos.clone()
        adj := this.adjustment
        
        pos.x += adj.x
        pos.y += adj.y
        pos.w += adj.w
        pos.h += adj.h

        fx := pos.x
        fy := pos.y
        fw := pos.w
        fh := pos.h

        if (!this.is_normal) {
            WinRestore, ahk_id %name%
        }

        WinMove, ahk_id %name%, , fx, fy, fw, fh
        wingetpos, wx, wy, ww, wh, ahk_id %name%

        e := 10

        cx := Abs(fx - wx) > e
        cy := Abs(fy - wy) > e
        cw := Abs(fw - ww) > e
        ch := Abs(fh - wh) > e
        
        if (cx or cy or cw or ch) {
            ;RajAhten().days.i("Aggressive human shoving", "=========")
            ;
            ; this works for corel draw:
            ;   it wont let you resize a window unless you also move it
            WinMove, ahk_id %name%, , fx + 1, fy + 1, fw, fh
            WinMove, ahk_id %name%, , fx, fy, fw, fh
        }

        
        this.stubborn_stop()
                        
    }

    brute_force_position() {
        this.stubborn_start()
        pos := this.pos
        name := this.name

        x := pos.x
        y := pos.y
        w := pos.w
        h := pos.h

        walnuts := 5

        y += walnuts
        h -= walnuts

        new_half_x := (w/2) + x
        new_half_y := (h/2) + y
        
        near := 50
        near_left_x := x + near
        near_left_y := new_half_y

        near_right_x := x + w - near
        near_right_y := new_half_y

        near_top_x := new_half_x
        near_top_y := y + near 

        near_bottom_x := new_half_x
        near_bottom_y := y + h - near

        WinGet, state, MinMax, ahk_id %name%
        if (state < 0 or state > 0) {
            WinRestore, ahk_id %name%
        }

        WinActivate, ahk_id %name%
        WinWaitActive, ahk_id %name%

        CoordMode, Mouse, Screen

        speed := 6
        sleepy := 20

        Gaborn().days.i("============================== Brute Force position ===", "speed", speed, "sleepy", sleepy)

        ;Send !{Space} 
        ;Send m
        ;Send {Left}
        ;MouseMove, x + (w/2), y, speed
        ;Click
        ;Sleep sleepy

        Send !{Space} 
        Send s
        Send {Left}
        MouseMove, near_left_x, near_left_y, speed
        sleep sleepy
        MouseMove, x, new_half_y, speed
        Click

        sleep sleepy

        Send, !{Space} 
        Send, {Down} 
        Send, {Down}
        Send, {Enter}
        Send, {Right}
        MouseMove, near_right_x, near_right_y, speed             
        sleep sleepy
        MouseMove, x + w, new_half_y, speed
        Click

        sleep sleepy

        Send, !{Space} 
        Send, {Down} 
        Send, {Down}
        Send, {Enter}
        Send, {Down}
        MouseMove, near_bottom_x, near_bottom_y, speed
        sleep sleepy
        MouseMove, new_half_x, y + h, speed
        Click

        sleep sleepy

        Send, !{Space} 
        Send, {Down} 
        Send, {Down}
        Send, {Enter}
        Send, {Up}
        MouseMove, near_top_x, near_top_y, speed
        sleep sleepy
        MouseMove, new_half_x, y, speed
        Click

        sleep sleepy

        MouseMove, new_half_x, new_half_y, 0

        this.stubborn_stop()        
    }
}

class WaterWizard {

    __new(a, b) {
        this.a := a
        this.b := b
        this.i := true
    } 

    poke() {
        this.i := (not this.i)
        ;EarthWarden().days.i("WaterWizard", "poke", this.i)
        return this.look()
    }

    look() {
        if (this.i) {
            ;EarthWarden().days.i("WaterWizard", "look", this.a)
            return this.a
        } 
        ;EarthWarden().days.i("WaterWizard", "look", this.b)        
        return this.b
    }


}

;
; Room of Adventure
;
class Kingdom {

    ; desktop GUID
    name := "" 

    active := ""
    last_active := ""
    
    people := new Circle()
    
    __new(name) {
        ;Gaborn().days.i("$$$$$$$$$$ new land", name)
        this.name := name
    }

    has(human) {
        return this.people.has_by_key(human, "name")
    }

    clear_dead_humans() {
        for i, human in this.people.elements() {
            human.update()
        }

        return this.people.remove_by_key_true("is_dead")
    }

    clear_travellers(locals) {
        travellers := []
        for i, name in locals {
            ;Gaborn().days.d("land", "clear_travellers", "has_by_key_equals", "name", name)            
            if (this.people.has_by_key_equals(name, "name")) {
                continue
            }
            ;Gaborn().days.d("land", "clear_travellers", "PUSH TRAVELLER", "name", name)      
            travellers.push(name)
        }

        for i, name in travellers {
            removed := this.people.remove_by_key_equals(name, "name")
            ;Gaborn().days.d("land", "clear_travellers", "remove_by_key_equals", "name", name, "removed", removed)
        }
    }

}