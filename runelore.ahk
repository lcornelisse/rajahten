
class AbstractSeal {
    static serial := 0

    type := AbstractSeal

    destroyed := false

    __new(hidden) {
        AbstractSeal.serial += 1
        this.name := AbstractSeal.serial

        this.runes_of_control := new Circle()
   
        this.ww_x_margin := new WaterWizard(0, 100)
        this.ww_y_margin := new WaterWizard(0, 100)

        this.ww_x_inner_margin := new WaterWizard(0, 50)
        this.ww_y_inner_margin := new WaterWizard(0, 50)
        
        this.x_margin := this.ww_x_margin.look()
        this.y_margin := this.ww_y_margin.look()

        this.x_inner_margin := this.ww_x_inner_margin.look()
        this.y_inner_margin := this.ww_x_inner_margin.look()

        this.x_bias := 0
        this.y_bias := 0

        this.desktop := 0

        this.content_changed := true
        this.spoiled := true

        this.hidden := hidden

        this.init()
    }

    __delete() {
        this.destroy()
    }

    destroy() {
        if (this.destroyed = false) {
            for i, rune in this.runes_of_control.elements() {
                rune.destroy()
            }

            this.runes_of_control.destroy()

            this.destroyed := true
        }
    }

    ; interface method - override this
    init() {
        RajAhten().days.w("AbstractSeal", "init")
    }

    new_window_hint() {
        RajAhten().days.w("AbstractSeal", "new_window_hint")
    }

    ; interface method - override this
    number_of_runes(num) {
        RajAhten().days.w("AbstractSeal", "number_of_runes")
    }

    ; interface method - override this
    calculate_positions() {
        RajAhten().days.w("AbstractSeal", "calculate_positions")
        return []
    }


    toggle_margins() {
        this.toggle_inner_margins()
        this.toggle_outer_margins()
    }

    toggle_inner_margins() {
        this.x_inner_margin := this.ww_x_inner_margin.poke()
        this.y_inner_margin := this.ww_y_inner_margin.poke()
    }
    
    toggle_outer_margins() {
        this.x_margin := this.ww_x_margin.poke()
        this.y_margin := this.ww_y_margin.poke()
    }

    hide_but_dont_resize_anything() {
        this.hidden := true
        this.hide_runes_but_dont_resize_anything()
    }

    hide() {
        this.hidden := true
        this.hide_runes()
    }

    show() {
        this.hidden := false
    }


    show_runes() {
        if (this.hidden = false) {
            for i, rune in this.runes_of_control.elements() {
                ;this.spoiled := true
                rune.show()
            }
            this.arrange_quicker()
        }
    }

    hide_runes() {
        for i, rune in this.runes_of_control.elements() {
            this.spoiled := false
            rune.hide()
        }
    }

    hide_runes_but_dont_resize_anything() {
        for i, rune in this.runes_of_control.elements() {
            this.spoiled := false
            rune.hide_but_dont_resize_anything()
        }
    }
    
  
    arrange() {
        ;RajAhten().days.d("arranging the seal")

        if (this.spoiled = false) {
            return
        } 

        pos := this.calculate_positions()
        rh := RuneOfControl.fixed_height
        n := 0

        for i, rune in this.runes_of_control.elements() {
            rune.pos.h := rh
            rune.pos.w := pos[i].w
            rune.pos.x := pos[i].x
            rune.pos.y := pos[i].y

            rune.human_pos.h := (pos[i].h - rh)
            rune.human_pos.w := pos[i].w
            rune.human_pos.x := pos[i].x
            rune.human_pos.y := (pos[i].y + rh)

            ;RajAhten().days.d("rune.human_pos", rune.human_pos.string())
            
            rune.human_pos_rune_hidden.h := pos[i].h
            rune.human_pos_rune_hidden.w := pos[i].w
            rune.human_pos_rune_hidden.x := pos[i].x
            rune.human_pos_rune_hidden.y := pos[i].y

            rune.inscribe()
        }

        ;for i, rune in this.runes_of_control.elements() {
        ;    if (i <= humans.length()) {
        ;        human := humans[i]
        ;        ;RajAhten().days.d("take human", human.title, rune.name)
        ;        rune.take_human(human)
        ;    } 
        ;}

        for i, rune in this.runes_of_control.elements() {     
            rune.position()
        }

        this.spoiled := false
        this.activate()
    }

    ;;
    ;; this one avoids changing buttons, just positions
    ;;
    arrange_quicker() {
        ;RajAhten().days.d("arranging the seal")

        pos := this.calculate_positions()

        rh := RuneOfControl.fixed_height

        for i, rune in this.runes_of_control.elements() {
            rune.pos.h := rh
            rune.pos.w := pos[i].w
            rune.pos.x := pos[i].x
            rune.pos.y := pos[i].y

            rune.human_pos.h := (pos[i].h - rh)
            rune.human_pos.w := pos[i].w
            rune.human_pos.x := pos[i].x
            rune.human_pos.y := (pos[i].y + rh)

            rune.human_pos_rune_hidden.h := pos[i].h
            rune.human_pos_rune_hidden.w := pos[i].w
            rune.human_pos_rune_hidden.x := pos[i].x
            rune.human_pos_rune_hidden.y := pos[i].y

            rune.position_quicker()
        }

        this.activate()
    }

    activate() {
        ;RajAhten().days.d("activating the seal")
    }

    whorl() {
        this.runes_of_control.whorl()
        this.arrange_quicker()
    }

    anti_whorl() {
        this.runes_of_control.anti_whorl()
        this.arrange_quicker()
    }

    whorl_inactive() {
        active_rune_i := 0
        ;RajAhten().days.d("whorl_inactive", active_rune_i)

        for i, rune in this.runes_of_control.elements() {
            human := rune.human
            if (human) {
                human.update()

                ;RajAhten().days.d("whorl_inactive", i, human.short, human.is_active)
                
                if (human.is_active) {
                    ;RajAhten().days.d("whorl_inactive", i, human.short, "ACTIVE")
                    active_rune_i := i
                    active_human := human
                }
            }
        }

        if (active_rune_i < 1) {
            active_rune_i := 1
        }

        this.runes_of_control.whorl_about(active_rune_i)
        this.arrange_quicker()

        active_human.attention()

    }
    
    anti_whorl_inactive() {
        active_rune_i := 0
        ;RajAhten().days.d("anti_whorl_inactive", active_rune_i)

        for i, rune in this.runes_of_control.elements() {
            human := rune.human
            if (human) {
                human.update()

                ;RajAhten().days.d("anti_whorl_inactive", i, human.short, human.is_active)
                
                if (human.is_active) {
                    ;RajAhten().days.d("anti_whorl_inactive", i, human.short, "ACTIVE")
                    active_rune_i := i
                    active_human := human
                }
            }
        }

        if (active_rune_i < 1) {
            active_rune_i := 1
        }
        
        this.runes_of_control.anti_whorl_about(active_rune_i)
        this.arrange_quicker()

        active_human.attention()
    }

    vacant_runes() {
        out := []
        for i, rune in this.runes_of_control.elements() {
            if (rune.human) {
                if (rune.human.is_dead) {
                    out.insertAt(1,rune)
                } else if (rune.human.rune != rune) {
                    out.insertAt(1,rune)
                } else if (rune.human.is_minimized) {
                    out.insertAt(1,rune)
                }
            } else {
                out.insertAt(1,rune)
            }
        }
        ;RajAhten().days.d("vacant_runes", out.length())
        return out
    }

    oustable_runes() {
        out := []
        humans := []
        last_active := EarthKing().land.last_active

        for i, rune in this.runes_of_control.elements() {
            if (rune.human) {
                humans.push(rune.human)
            }
        }
        ; sort in reverse using active time sorting hat
        Sort, humans, R F Human_ActiveTimeSortingHat

        for i, human in humans {
            ;RajAhten().days.i("oustable_runes", "sorted humans", i, human.last_active)
            if (i > 1 and human.is_active = false) {
                out.insertAt(1,rune)
            }
        }
        ;RajAhten().days.d("oustable_runes", "out", out.length())
        ;for i, o in out {
        ;    RajAhten().days.d("oustable_runes", "out", i, o.string())
        ;}
        return out
    }

    sorted_runes() {
        humans := []
        last_active := EarthKing().land.last_active

        for i, rune in this.runes_of_control.elements() {
            if (rune.human) {
                humans.push(rune.human)
            }
        }
        ; sort in reverse using active time sorting hat
        Sort, humans, R F Human_ActiveTimeSortingHat

        ;RajAhten().days.i("sorted_runes", humans.length(), "=========")
        ;for i, h in humans {
        ;    RajAhten().days.i("sorted_runes", i, h.string())
        ;}
        
        return humans
    }

    clear_travellers(locals) {
        ;RajAhten().days.i("seal.clear_travellers", locals.length())

        for i, rune in this.runes_of_control.elements() {
            ;RajAhten().days.i("seal.clear_travellers", "rune", rune.name, "human", rune.human.short)
            
            if (rune.human) {
                name := rune.human.name
                not_found := true
                for j, x in locals {
                    if (x = name) {
                        not_found := false    
                    } 
                    ;RajAhten().days.i("seal.clear_travellers", "NOT", rune.human.short, name, x, "not found", not_found )
                }
                if (not_found) {
                    ;RajAhten().days.i("seal.clear_travellers", "OH SHIT WHADDUP?", rune.human.short, "CLEARING HUMAN")
                    
                    rune.human := ""
                    this.spoiled := true
                } 
            }
        }

    }

}


class SealOfInferno extends AbstractSeal {

    type := SealOfInferno

    init() {
    }

    new_window_hint() {
        return this.runes_of_control.at_back()
    }

    number_of_runes(num) {

        while (this.runes_of_control.length() < num) {
            ;RajAhten().days.i("new RuneOfControl")
            x := new RuneOfControl(this.hidden)
            this.runes_of_control.push(x)
            this.spoiled := true
        }

        while (this.runes_of_control.length() > num) {
            x := this.runes_of_control.pop()
            x.destroy()
            this.spoiled := true
        }

    }

    calculate_positions() {

        num_runes := this.runes_of_control.length()
        active := EarthKing().land.active

        current := EarthWarden().getWindowsMonitorNumber(active.name)
        SysGet, Mon, MonitorWorkArea, current 

        MX := this.x_margin
        MY := this.y_margin

        inner_MX := this.x_inner_margin
        inner_MY := this.y_inner_margin
        
        half_W := (MonRight - MonLeft) /2
        half_H := (MonBottom - MonTop) /2

        W := MonRight - MonLeft - MX - MX
        W_small := half_W - MX - inner_MX

        H := MonBottom - MonTop - MY - MY
        H_small := half_H - MY - inner_MY

        ; resize with bias
        ;W += this.x_bias
        ;H += this.y_bias

        half_W += this.x_bias
        half_H += this.y_bias

        W_small_left  := W_small + this.x_bias
        W_small_right := W_small - this.x_bias

        H_small_top    := H_small + this.y_bias
        H_small_bottom := H_small - this.y_bias

        ; top left
        X1 := MonLeft + MX
        Y1 := MonTop + MY
    
        ; top right
        X2 := half_W + inner_MX
        Y2 := MonTop + MY
    
        ; bottom right    
        X3 := half_W + inner_MX
        Y3 := half_H + inner_MY

        ; bottom left
        X4 := MonLeft + MX
        Y4 := half_H + inner_MY
        
        if (num_runes = 1) {
            W1 := W
            H1 := H

        } else if (num_runes = 2) {

            W1 := W_small_left
            H1 := H

            W2 := W_small_right
            H2 := H

        } else if (num_runes = 3) {
            
            W1 := W_small_left
            H1 := H

            W2 := W_small_right
            H2 := H_small_top
        
            W3 := W_small_right
            H3 := H_small_bottom

        } else if (num_runes = 4) {
            
            W1 := W_small_left
            H1 := H_small_top

            W2 := W_small_right
            H2 := H_small_top
            
            W3 := W_small_right
            H3 := H_small_bottom
            
            W4 := W_small_left
            H4 := H_small_bottom
        }

        out := []

        Loop, % num_runes 
        {
            out.push(new Dimensions(X%a_index%, Y%a_index%, W%a_index%, H%a_index%))
        } 

        return out
    }

}

class SealOfPoisonwood extends AbstractSeal {

    type := SealOfPoisonwood

    init() {
        loop, 3 {
            this.runes_of_control.push(new RuneOfControl(this.hidden))
        }
    }

    rune_main() {
        return this.runes_of_control.at(1)
    }

    rune_right() {
        return this.runes_of_control.at(2)
    }

    rune_bottom() {
        return this.runes_of_control.at(3)
    }

   

    number_of_runes(num) {
    }

    new_window_hint() {
        return this.rune_right()
    }

    new_window_hint_i() {
        return this.rune_right()
    }

    calculate_positions() {

        num_runes := this.runes_of_control.length()
        active := EarthKing().land.active

        current := EarthWarden().getWindowsMonitorNumber(active.name)
        SysGet, Mon, MonitorWorkArea, current 

        MX := this.x_margin
        MY := this.y_margin

        inner_MX := this.x_inner_margin
        inner_MY := this.y_inner_margin
  
        half_W := (MonRight - MonLeft) /2
        half_H := (MonBottom - MonTop) /2

        third_W := (MonRight - MonLeft) /3
        third_H := (MonBottom - MonTop) /3

        quarter_W := (MonRight - MonLeft) /4
        quarter_H := (MonBottom - MonTop) /4

        fifth_W := (MonRight - MonLeft) /5
        fifth_H := (MonBottom - MonTop) /5

        W := MonRight - MonLeft - MX - MX
        W_left := third_W * 2 - MX - inner_MX
        W_right := third_W - MX - inner_MX

        H := MonBottom - MonTop - MY - MY
        H_top := quarter_H * 3 - MY - inner_MY
        H_bottom := quarter_H - MY - inner_MY

        ; resize with bias
        ;half_W += this.x_bias
        ;half_H += this.y_bias

        ;third_W += this.x_bias
        ;third_H += this.y_bias

        W_left  := W_left + this.x_bias
        W_right := W_right - this.x_bias

        H_top    := H_top + this.y_bias
        H_bottom := H_bottom - this.y_bias

        ; top left
        X1 := MonLeft + MX
        Y1 := MonTop + MY
    
        ; top right
        X2 := third_W * 2 + inner_MX + this.x_bias
        Y2 := MonTop + MY 
    
        ; bottom    
        X3 := X1
        Y3 := quarter_H * 3 + inner_MY + this.y_bias

        ; bottom left
        ;X4 := MonLeft + MX
        ;Y4 := half_H + half_MY
        
        W1 := W_left
        H1 := H_top

        W2 := W_right
        H2 := H_top
    
        W3 := W
        H3 := H_bottom

        out := []

        Loop, % num_runes 
        {
            ;RajAhten().days.i("calc pos", %a_index%, "--", X%a_index%, Y%a_index%, W%a_index%, H%a_index%)
            out.push(new Dimensions(X%a_index%, Y%a_index%, W%a_index%, H%a_index%))
        } 

        return out
    }


    on_new_human() {
    }

    on_newly_active_human() {
    }

    on_human_resize() {
    }

    place_by_numbers() {
    }



    swap_left() {
    }

    swap_right() {
    }

    swap_up() {
    }

    swap_down() {
    }



}