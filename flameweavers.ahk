O_Great_Light() {
    return RajAhten.instance
}
Flameweavers() {
    return Flameweavers.instance
}
class Flameweavers {
    static instance := new Flameweavers()

    graak := ""

    spy_graaks := []
    ready := true

    dead_graaks := []
    
    SirBorensonsWalnuts := 0

    busy := false

    
    __new() {
    }

    watch_for_changes() {

        ;days := RajAhten().days

        if (this.ready = false) {
            return
        }

        if (this.busy = true) {
            return
        }
        
        Critical, On
        this.busy := true
        this.ready := false

        do_nothing := false
        
        graak := this.find_spy_graak()

        ;days.d("watch_for_changes", "graak", graak)
        if (graak = "") {
            this.SirBorensonsWalnuts += 1
            ;days.d("watch_for_changes", "SirBorensonsWalnuts", this.SirBorensonsWalnuts)

            if (this.SirBorensonsWalnuts = 2) {
                ;days.d("watch_for_changes", "launching graak")
                
                graak := this.launch_spy_graak()
                this.SirBorensonsWalnuts := 0
            } else {
                do_nothing := true
                ;days.d("watch_for_changes", "do_nothing", do_nothing)
            }

        } else {
            this.SirBorensonsWalnuts := 0
        }
        Critical, Off

        if (do_nothing) {
            this.ready := true
            this.busy := false
            return
        }

        if (graak != this.graak) {
            this.graak := graak
            O_Great_Light().land_has_changed(graak)
        
        } else {
            O_Great_Light().tick(graak)
        }

        this.ready := true
        this.busy := false

        return graak

    }

    stop_watching() {
        this.ready := false
    }

    launch_spy_graak() {
        Gui, new, +Hwndgraak, % "Graak"
		Gui, %graak%:show, NA
	
		this.spy_graaks.push(graak)
        return graak
    }

    find_spy_graak() {

        current := O_Great_Light().graak
        found := EarthWarden().isWindowOnCurrentVirtualDesktop(current)

        if (found) {
            return current
        }

        for i, graak in this.spy_graaks {

            IfWinNotExist, ahk_id %graak%
            {
                dead_graaks.push(graak)
                continue
            } 
            Gui, %graak%:show, NA

            found := EarthWarden().isWindowOnCurrentVirtualDesktop(graak)
            
            if (found) {
                return graak
            }
        }

        ;msgbox, % "no spy graak found"
        return ""
    }
}
