Spring() {
    return Wylde.instance
}

class Wylde {
	static instance := new Wylde()
    days := new Days("Spring")
    
	__new() {
        this.days.i("Blood, yes?")
    }
    
    __delete() {
    }   

    hunt() {

        ;EarthKing().choose_runes()

        runes := []
        graaks := []

        WinGet, id, list, ahk_class AutoHotkeyGUI,,
        Loop, %id%
        {
            x := id%a_index%

            WinGetTitle title, ahk_id %x%

            if (title = "Rune of Control") {
                runes.push(x)
            }

            if (title = "Graak") {
                graaks.push(x)
            }
            
        }

       
        for i, graak in graaks {
            found := EarthWarden().isWindowOnCurrentVirtualDesktop(graak)
            if (found > 0) {
                blood_no := RajAhten().current_graak
                if (graak != blood_no) {
                    ;msgbox % "Blood yes! graak: " . graak
                    Gui, %graak%:Destroy
                    ;RajAhten().graak_down(graak)
                }
            }
        }

        for i, rune in runes {
            seals := RajAhten().seals
            blood_yes := true
            ;msgbox, % "i " . i
            for j, seal in seals {
               ; msgbox, % "j " . j
                for k, seal_rune in seal.runes_of_control.elements() {
                ;    msgbox, % "k " . k
                    if (seal_rune.name = rune) {
                        
                        blood_yes := false
                        break
                    }
                }

                if (blood_yes = false) {
                    break
                }
            }

            if (blood_yes) {
                ;msgbox % "Blood yes! rune: " . rune
                try {
                    ;Gui, %rune%:Destroy
                }
            }
            
        }

    }

}