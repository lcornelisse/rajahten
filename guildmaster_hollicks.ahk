;
; Guildmaster Hollicks, Master of the Dyer's Guild
;

GuildmasterHollicks() {
    return GuildmasterHollicks.instance
}
class GuildmasterHollicks {

    static instance := new GuildmasterHollicks()
    
    class_wardrobe := {}
    exe_wardrobe := {}

    __new() {
        w := this.exe_wardrobe
        w["opera.exe"] := new Opera_Clothes()
        w["Code.exe"] := new VSCode_Clothes()
        w["ConEmu64.exe"] := new ConEmu_Clothes() 
        w["Dominion.exe"] := new Dominion_Clothes() 
        w["PDCfg.exe"] := new RedDragon_Clothes()
    }

    outfit(human) {
        exe := human.exe
        outfit := this.exe_wardrobe[exe]

        if (!outfit) {
            outfit := new Clothes()
            ;Gaborn().days.i("Outfitting human", human.exe, "new outfit", outfit.string)
            this.exe_wardrobe[exe] := x
        }
        ;Gaborn().days.i("Outfitting human", human.exe, outfit.str)
        
        this.dress(human, outfit)
    }

    dress(human, clothes) {

        if (!human.clothes) {

            human.clothes := clothes

            if (clothes.has_caption) {
                ;Gaborn().days.i("Outfitting human", "showing caption")                
                ;human.show_caption()
            } else {
                ;Gaborn().days.i("Outfitting human", "hiding caption", clothes.str, clothes.has_caption)
                human.hide_caption()
            }

            ;human.transparency(clothes.transparency)

            if (clothes.always_maximized) {
                ;Gaborn().days.i("Outfitting human", human.name, "maximizing")                
                human.maximize()
            }

            ; gravity

            ; margins

            ; x y bias
        } else {
            ;Gaborn().days.i("dressing human, already dressed.", human.exe)
        }
    }

}

class Clothes {

    has_caption := true
    transparency := 0

    str := "Plain tunic with embroidered Green Man."

    always_maximized := false

    allow_maximize := true
    
    resize_by := false
    gravity := new Dimension(0,0,0,0)
    margins := new Dimension(0,0,0,0)
    inner_margins := new Dimension(0,0,0,0)

    fixed := false
    fixed_size := new Dimension(0,0,0,0)


    x_bias := 0
    y_bias := 0

    pants := true

    ;__new() {
    ;    Gaborn().days.i("New Clothes", this.str)
    ;}

}

class Opera_Clothes extends Clothes {
    str := "Opera"
    allow_maximize := false
}

class ConEmu_Clothes extends Clothes {
    str := "ConEmu"
    allow_maximize := false
    ;has_caption := false
}

class VSCode_Clothes extends Clothes {
    str := "VSCode"
    has_caption := false
    resize_by := new Dimensions(0, 0, 0, -5)
}

class Dominion_Clothes extends Clothes {
    str := "Dominion"
    has_caption := false
    resize_by := new Dimensions(0, 0, 0, 5)

    margins := new Dimension(200, 100, 0, 0)

}

class RedDragon_Clothes extends Clothes {
    str := "RedDragon"
    fixed := true
    fixed_size := new Dimension(0, 0, 1030, 840)
}
