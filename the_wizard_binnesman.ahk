EarthWarden() {
    return Binnesman.instance
}
Binnesman() {
    return Binnesman.instance
}
class Binnesman {
	static instance := new Binnesman()
    static ignoreList := WorkerW,TaskListOverlayWnd

    days := new Days("The Wizard Binnesman")

	__new() {
        this.days.i("Oh for the.. I have a Days now?")
    }

    __delete() {
        
    }

    what_do_you_see() {
        s := ""
        all_windows := this.collect_all_windows()
        cloaked := []
        on_screen := []

        for i, hwnd in all_windows {
            human := new SeedOfHumanity(hwnd)

            if (this.isWindowCloaked(hwnd)) {
                cloaked.push(human)
            } else {
                found := this.isWindowOnCurrentVirtualDesktop(hwnd)

                if (found < 0) {
                    s := s . "Error " . found . " inspecting " . human.exe . " :: " . human.short . " :: " . human.class . "`n"
                } else if (found > 0) {
                    on_screen.push(human)
                }

            }
        }
        
        s := s . "`nCloaked Humans:`n"
        for i, human in cloaked {
            s := s . "   - " . human.exe . " :: " . human.short . " :: " . human.class . "`n"  
        }

        s := s . "`nOnScreen Humans:`n"
        for i, human in on_screen {
            s := s . "   - " . human.exe . " :: " . human.short . " :: " . human.class . "`n"  
        }

        s := s . "`nXming Humans:`n"
        for i, human in on_screen {
            if (human.exe = "Xming.exe") {
                s := s . human.dump() . "`n"
            }                
        }
        
        this.msgbox("The Wizard Binnesman sees", s)

    }

    msgbox(title, string) {
        ScrollBox(string, "b1 f{s12 cBlack, Consolas}", title) 
    }

    ;;
    ;; The Wolf Lord made the wizard Binnesman take an endowment of wit from Flipeador of the AHK forums. 
    ;; It was either that, or he'd start fraking Mystarria for blood metal. 
    ;; 
    ;Indicates whether the provided window is on the currently active virtual desktop.
    isWindowOnCurrentVirtualDesktop(hwnd) {
        ;this.days.d("isWindowOnCurrentVirtualDesktop?", hwnd)
        
        ;IVirtualDesktopManager interface
        ;Exposes methods that enable an application to interact with groups of windows that form virtual workspaces.
        ;https://msdn.microsoft.com/en-us/library/windows/desktop/mt186440(v=vs.85).aspx
        CLSID := "{aa509086-5ca9-4c25-8f95-589d3c07b48a}" ;search VirtualDesktopManager clsid
        IID := "{a5cd92ff-29be-454c-8d04-d82879fb3f1b}" ;search IID_IVirtualDesktopManager
        IVirtualDesktopManager := ComObjCreate(CLSID, IID)

        ;IVirtualDesktopManager::IsWindowOnCurrentVirtualDesktop method
        ;Indicates whether the provided window is on the currently active virtual desktop.
        ;https://msdn.microsoft.com/en-us/library/windows/desktop/mt186442(v=vs.85).aspx
        Error := DllCall(NumGet(NumGet(IVirtualDesktopManager+0), 3*A_PtrSize), "Ptr", IVirtualDesktopManager, "Ptr", hwnd, "IntP", onCurrentDesktop)
    
        ;free IVirtualDesktopManager
        ObjRelease(IVirtualDesktopManager)

        if (Error = 0) {
            ;this.days.d("onCurrentDesktop", onCurrentDesktop)
            return onCurrentDesktop, ErrorLevel := false
        }
        WinGetClass, klass, % "ahk_id" hwnd
        this.days.e("isWindowOnCurrentVirtualDesktop", klass, "Error", Error)
        return Error, ErrorLevel := true
    }

    getWindowDesktopId(hwnd, tryAgain := true) {
        ;this.days.d("getWindowDesktopId?", hwnd)
        
        ;IVirtualDesktopManager interface
        ;Exposes methods that enable an application to interact with groups of windows that form virtual workspaces.
        ;https://msdn.microsoft.com/en-us/library/windows/desktop/mt186440(v=vs.85).aspx
        CLSID := "{aa509086-5ca9-4c25-8f95-589d3c07b48a}" ;search VirtualDesktopManager clsid
        IID := "{a5cd92ff-29be-454c-8d04-d82879fb3f1b}" ;search IID_IVirtualDesktopManager
        IVirtualDesktopManager := ComObjCreate(CLSID, IID)

        desktopId := ""
		VarSetCapacity(desktopID, 16, 0)
		
        ;IVirtualDesktopManager::GetWindowDesktopId  method
		;https://msdn.microsoft.com/en-us/library/windows/desktop/mt186441(v=vs.85).aspx
        Error := DllCall(NumGet(NumGet(IVirtualDesktopManager+0), 4*A_PtrSize), "Ptr", IVirtualDesktopManager, "Ptr", hwnd, "Ptr", &desktopID)	
		
        ;free IVirtualDesktopManager
        ObjRelease(IVirtualDesktopManager)

        if(Error != 0) {
			if(tryAgain) 
			{
				return this.getWindowDesktopId(hwnd, false)
			}
			msgbox % "error in getWindowDesktopId " Error
			clipboard := error
		}
 
        pGuid := &desktopID
		VarSetCapacity(sGuid, 78) ; (38 + 1) * 2
		if !DllCall("ole32\StringFromGUID2", "Ptr", pGuid, "Ptr", &sGuid, "Int", 39)
			throw Exception("Invalid GUID", -1, Format("<at {1:p}>", pGuid))
		
        return StrGet(&sGuid, "UTF-16")

    }

    getCurrentDesktop() {
        msgbox % globalDesktopManager
        return globalDesktopManager._desktopChanger.desktopMapper.getCurrentDesktop()
    }

    ; Windows Metro apps can be a new kind of hidden: cloaked. The wizard Binnesman is clever enough to know the difference.
    isWindowCloaked(hWindow) {
        Static Dummy5693
            ,isCloaked
            ,S_OK:=0x0
            ,DWMWA_CLOAKED:=14

        ;-- Workaround for AutoHotkey Basic
        PtrType:=(A_PtrSize=8) ? "Ptr":"UInt"

        VarSetCapacity(isCloaked,4,0)
        DWMRC:=DllCall("dwmapi\DwmGetWindowAttribute"
            ,PtrType,hWindow                     ;-- hwnd
            ,"UInt",DWMWA_CLOAKED                ;-- dwAttribute
            ,PtrType,&isCloaked                  ;-- pvAttribute
            ,"UInt",4)                           ;-- cbAttribute

        if (DWMRC<>S_OK) {
            return 0
        }
        return NumGet(isCloaked, 0, "UInt")
    }

    
    collectOnScreenWindows() {
        onScreenWindows := Object()
        
        WinGet, id, list,,, Program Manager
        Loop, %id%
        {
            this_id := id%A_Index%

            isCloaked := this.isWindowCloaked(this_id)
            if (isCloaked = 0) {
                WinGetTitle, this_title, ahk_id %this_id%
                WinGetClass, this_class, ahk_id %this_id%

                ; The Earth has warned Gaborn that he chooses too freely. Here we make his job easier.
                if this_class not in WorkerW,TaskListOverlayWnd,Shell_TrayWnd,Internet Explorer_Hidden
                    ,TApplication,AutoHotkeyGUI,ToolTips_class32,SysShadow,FrameTab Alternate Owner,Frame Alternate Owner
                    ,Alternate Owner,Chrome_WidgetWin_2,Net UI Tool Window Layered,#32770,32770
                {
                    ;this.days.d("hmmm..", this_class)

                    found := this.isWindowOnCurrentVirtualDesktop(this_id)              
                    if (found > 0) {
                        onScreenWindows.push(this_id)
                    }

                } else {
                    ;this.days.d("ignoring", this_class, this_title)
                }
            }
        }
        return onScreenWindows
    }

    collect_all_windows() {
        all_windows := []
        
        WinGet, id, list,,, Program Manager
        Loop, %id%
        {
            this_id := id%A_Index%

            all_windows.push(this_id)
            
        }
        return all_windows
    }


    getWindowsMonitorNumber(hwnd) {
        WinGetPos, x, y, width, height, % "Ahk_id" hwnd
        ;debugger("Window Position/Size:`nX: " X "`nY: " Y "`nWidth: " width "`nHeight: " height)	
        SysGet, monitorcount, MonitorCount
        SysGet, primarymonitor, MonitorPrimary	
        ;debugger("Monitor Count: " MonitorCount)	
        Loop %monitorcount%
        {
            SysGet, mon, Monitor, %a_index%
            ;debugger("Primary Monitor: " primarymonitor "`nDistance between monitor #" a_index "'s right border and Primary monitor's left border (Left < 0, Right > 0):`n" monRight "px")	
            If (x < monRight - width / 2 || monitorcount = a_index) {
                return %a_index%
            }
        }
    }
}