
class IEarthMagic {
    rune := ""

    call() {
    }

}

class HumanEarthMagic {

    __new(rune, human) {
        this.rune := rune
        this.human := human
    }

    call() {
        ;days := RajAhten().days
        ;days.i("*********************************")
        ;days.i("*** click ***********************")

        human := this.human
        rune := this.rune

        ;days.d("human", human.short, "rune", rune.string())

        old_rune := human.rune
        old_human := rune.human

        if (old_rune and old_rune.is_destroyed = false) {
            if (old_rune.name != rune.name) {
                ;days.d("old_rune and old_rune.is_destroyed = false", old_rune.name)
                old_rune.take_human(old_human)
            } else {
                if (old_human) {
                    ;days.d("same rune!", "old_human", old_human.short)                
                    old_human.rune := ""
                    old_human.in_danger := true  
                }
            }
        } else {
            if (old_human) {
                ;days.d("destroyed rune!", "old_human", old_human.short)                
                old_human.rune := ""  
                old_human.in_danger := true                  
            }
        }
            
        ;days.d("rune.take_human(human)")
        rune.take_human(human)
        ;days.d("rune.activate_human(human)")
        ;rune.activate_human(human)

        ; manipulation!
        human.in_danger := false
        human.feeling_scared := false
        human.attention()
        
        ;days.d("rune.position()")
        rune.position()

        if (old_rune and old_rune.is_destroyed = false and old_rune.name != rune.name) {
            ;days.d("old_rune and old_rune.is_destroyed = false and old_rune.name != rune.name")
            old_rune.position()
        }    
        ;days.i("*********************************")
        
        return
    }

}


class BiasMagic {

    __new(rune) {
        this.rune := rune
        this.derp := "BiasMagic"
    }

    drag(dx, dy) {
        ;days := RajAhten().days
        ;days.d("*** bias -------------------****", dx, dy)
        
        seal := RajAhten().seal

        seal.x_bias += dx
        seal.y_bias += dy

        seal.arrange_quicker()

        ;days.d("****------------------------****")
        return
    }

    click() {
        seal := RajAhten().seal

        pos := this.rune.get_adjusted_human_pos()
        
        if (pos.w < pos.h) {
            seal.x_bias := pos.h - pos.w
        } else {
            seal.x_bias := pos.w - pos.h
        }   

        seal.arrange_quicker()

    }
}

class MarginsMagic {

    __new(rune) {
        this.rune := rune
        this.derp := "MarginsMagic"
    }

    drag(dx, dy) {
        ;days := RajAhten().days
        ;days.d("*** bias -------------------****", dx, dy)
        
        seal := RajAhten().seal

        shift := GetKeyState("Shift", "P")
        ctrl := GetKeyState("Ctrl", "P")

        if (shift and ctrl) {
            seal.x_margin -= dx
            seal.y_margin += dy
            seal.x_inner_margin -= (dx/2)
            seal.y_inner_margin += (dy/2) 

        } else if (shift) {
            seal.x_inner_margin -= (dx/2)
            seal.y_inner_margin += (dy/2) 

        } else if (ctrl) {
            seal.x_margin -= dx
            seal.y_margin += dy

        } else {
            seal.x_margin -= dx
            seal.y_margin += dy
            seal.x_inner_margin -= (dx/2)
            seal.y_inner_margin += (dy/2)
        }


        if (seal.x_margin < 0) {
            seal.x_margin := 0
        }

        if (seal.y_margin < 0) {
            seal.y_margin := 0
        }

        if (seal.x_inner_margin < 0) {
            seal.x_inner_margin := 0
        }

        if (seal.y_inner_margin < 0) {
            seal.y_inner_margin := 0
        }


        seal.arrange_quicker()

        ;days.d("****------------------------****")
        return
    }

    click() {
        seal := RajAhten().seal
        shift := GetKeyState("Shift", "P")
        ctrl := GetKeyState("Ctrl", "P")

         if (shift and ctrl) {
            seal.toggle_margins()

        } else if (shift) {
            seal.toggle_inner_margins() 

        } else if (ctrl) {
            seal.toggle_outer_margins()

        } else {
            seal.toggle_margins()
        }

        
        seal.arrange_quicker()
    }

}

class LocusMagic {

    __new(rune) {
        this.rune := rune
        this.derp := "LocusMagic"
    }

    drag(dx, dy) {
        ;days := RajAhten().days
        ;days.d("*** locus drag -------------------****", dx, dy)
       
        ;days.d("****------------------------****")
        return
    }

    click() {
        ;days := RajAhten().days
        human := this.rune.human
        ;days.d("*** locus click -------------------****", human.string(), human.pos.String(), human.pos)
        
        RajAhten().seal.arrange_quicker()
    }

}

class InvincibleMagic {

    __new(rune) {
        this.rune := rune
        this.derp := "InvincibleMagic"
    }

    drag(dx, dy) {
        ;days := RajAhten().days
        ;days.d("*** bias -------------------****", dx, dy)
        ;days.d("****------------------------****")
        return
    }

    click() {
        this.rune.human.brute_force_position()
    }
}

class InfernoMagic {

    __new(rune, number) {
        this.rune := rune
        this.number := number
        this.derp := "InfernoMagic"
    }

    drag(dx, dy) {
        ;days := RajAhten().days
        ;days.d("*** bias -------------------****", dx, dy)
        ;days.d("****------------------------****")
        return
    }

    click() {
        RajAhten().change_seal(this.number)
    }
}



class HidingMagic {

    __new(rune) {
        this.rune := rune
        this.derp := "HidingMagic"
    }

    drag(dx, dy) {
        ;days := RajAhten().days
        ;days.d("*** hiding -------------------****", dx, dy)
        ;days.d("****------------------------****")
        return
    }

    click() {
        RajAhten().hide()
    }
}

class HideCaptionMagic {

    __new(rune) {
        this.rune := rune
        this.derp := "HideCaptionMagic"
    }

    drag(dx, dy) {
        ;days := RajAhten().days
        ;days.d("*** hiding -------------------****", dx, dy)
        ;days.d("****------------------------****")
        return
    }

    click() {
        this.rune.human.toggle_caption()
    }
}
