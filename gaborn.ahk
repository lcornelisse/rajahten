
EarthKing() {
    return Gaborn.instance
}
Gaborn() {
    return Gaborn.instance
}
class Gaborn {
	static instance := new Gaborn()
    days := new Days("Gaborn")
    
    people := {}
    newcomers := []
    army := []

    lands := {}
    land := ""
    land_changed := false

	__new() {
        this.days.i("The Earth King licked his lips.")
    }
    
    __delete() {
    }

    earth_sight(name) {
        ;this.days.i("Using Earth Sight", "name", name)
        x := this.people[name]

        if (!x) {
            ;this.days.i("Gaborn does not know of this one")
            y := new SeedOfHumanity(name)
            this.people[name] := y
            ;this.days.i("Human:", y.name, y.title)
            return y
        }

        x.update()
        ;this.days.i("Human:", x.name, x.title)
        
        ; we want to only do this once per turn, by earth_sight, so 
        ; we don't lose if the state has changed, as updates 
        ; happen elsewhere
        x.update_minmax()

        return x
    }

    ; graak is the hwnd of Raj Ahten's Flameweavers' graak shaped spy balloon. 
    land_has_changed(graak) {
        land := this.lands[graak]
        if (!land) {
            land := new Kingdom(EarthWarden().getWindowDesktopId(graak))        
            this.lands[graak] := land
        }
        this.land := land
        this.army := land.people.elements()
        this.land_changed := true
        return land
    }

    ; graak is the hwnd of Raj Ahten's Flameweavers' graak shaped spy balloon. 
    watch_for_changes(graak) {

        active := false

        ;this.days.d("watch_for_changes")

        if (this.land_changed) {
            ; make new active
            humans := []

            seal := RajAhten().seal
            ;this.days.d("watch_for_changes", "===", seal.type)
            
            runes := seal.runes_of_control.elements()
            ;this.days.d("watch_for_changes", "---n", runes.length())

            for i, rune in runes {
                human := rune.human
                ;this.days.d("watch_for_changes", "derp", human.short)
                here := EarthWarden().isWindowCloaked(human.name) = false
                if (human and here) {
                    ;this.days.d("watch_for_changes", "derp zerp zorp", human.short)                
                    humans.push(human)
                }
            }
            ; sort in reverse using active time sorting hat
            Sort, humans, R F Human_ActiveTimeSortingHat

            active := humans[1]    
        
        } else {
        
            for i, human in this.army {

                if (EarthWarden().isWindowOnCurrentVirtualDesktop(human.name)) {

                    if (human.in_danger) {
                        ;this.days.d("***", human.short, "HIDE!")
                        human.hide()
                    } else {
                        ;this.days.d("***", human.short, "STRIKE!")                
                        human.strike()
                    }

                    if (human.is_active) {
                        active := human
                    }
                }
            }
        }

        if (active) {
            active.attention()
        }

        this.land_changed := false
    }

    your_move() {
        this.assign(this.army)
    }

    ; rename to something like the earth reclaims the dead
    clear_dead_humans() {        
        return this.land.clear_dead_humans()
    }

    clear_travellers(locals) {
        this.land.clear_travellers(locals)
        RajAhten().seal.clear_travellers(locals)
    }

    choose() {
        this.newcomers := []
        land := this.land
        
        standing_before_me := EarthWarden().collectOnScreenWindows()
        
        this.clear_travellers(standing_before_me)

        for i, name in standing_before_me {
            human := this.earth_sight(name)
            ;this.days.d("CHOOSE", "used earth sight", name, human.short, human.rune)

            ;; I choose you. 
            ;; I choose you for the Earth. 
            ;; May the Earth hide you.
            ;; May the Earth heal you.
            ;; May the Earth make you its own.

            if (human.title and human.title != "Graak") {
                if (land.has(human) = false) {
                    land.people.push(human)
                    ;this.days.d("CHOOSE", "human new to this land", human.short, human.rune)

                    if (human.rune) {
                        human.rune := ""
                    }

                    this.newcomers.push(human)
                    GuildmasterHollicks().outfit(human)

                } else {
                    if (human.minmax_state_changed) {
                        this.newcomers.push(human)
                        ;this.days.d("CHOOSE", "MIN MAX CHANGED", human.short, human.minmax_state_changed)

                        if (human.is_minimized and human.in_danger = false) {
                            human.feeling_scared := true
                        }

                        if (human.rune and human.rune.is_destroyed) {
                            human.rune := ""
                        } 

                        if (human.feeling_scared) {
                            human.rune := ""
                        }
                        ;this.days.d("CHOOSE", ((human.feeling_scared) ? "FEELS SCARED" : "NOT AFRAID!"), human.feeling_scared)
                        
                    }
                }
            }
        }

        return this.newcomers
    }
 
    assign(humans) {

        seal := RajAhten().seal
        n := seal.runes_of_control.length()
      
        runes := seal.runes_of_control.elements()

        newcomers := this.newcomers
        newcomers_n := this.newcomers.length()

        ; find active human

        active_human := false
        active_human_is_new := false

        ; is active human new?
        for i, human in newcomers {
            if (human.is_active) {
                active_human := human
                active_human_is_new := true
            }
        }

        if (!active_human) {
            for i, human in humans {
                if (human.is_active) {
                    active_human := human
                }
            }
        }

        ;if (active_human) {
            ;this.days.d("assign", 0, "active_human", active_human.short)            
        ;    if (active_human.rune) {
        ;        if (active_human.rune.is_destroyed) {
        ;            active_human_spot := false
        ;            active_human.rune := ""
        ;        } else {
        ;            for x, rune in runes {
        ;                if (rune = active_human.rune) {
        ;                    active_human_spot := x
        ;                    break
        ;                }
        ;            }    
        ;        }
        ;    }
        ;}

        
        
        ; what was the last active human?
        ; where were they? get index.
        ;last_active_human := false
        ;last_active_human_spot := false

        ;for x, rune in runes {
        ;    if (rune = land.last_active.rune) {
        ;        last_active_human := rune.human
        ;        last_active_human_spot := x
        ;        break
        ;    }
        ;}

        ; find spots, spots aren't runes, runes move. spots are indexes.
        ;  active window
        ;  last active window
        ;  vacant spots
        ;  custom spots
        ;    human may want:
        ;      tallest spot
        ;      widest spot
        ;      specific spot
        ;      no spot - a knight equitable

        vacancies := seal.vacant_runes()
        oustables := seal.oustable_runes()

        sorted_runes := seal.sorted_runes()

        j := 0
        attacking := 0

        not_this_one := 0
        
        land := this.land

        ;this.days.d("assign", 0, "=========")
        ;sorted_humans := []
        ;for i, h in humans {
        ;    this.days.i("assign", "unsorted", i, h.string())
        ;    sorted_humans.push(h)
        ;}

        ; sort in reverse using active time sorting hat
        ;Sort, humans, R F Human_ActiveTimeSortingHat
        ;Human_ActiveTimeSortingHat("derp","dorp")
        ;Sort, sorted_humans, "F Human_ActiveTimeSortingHat"
        ;for i, h in sorted_humans {
        ;    this.days.i("assign", "sorted", i, h.string())
        ;}

        humans_circle := new Circle()
        humans_circle.wrap(humans)
        
        ;for i, h in humans_circle.elements() {
        ;    this.days.i("assign", "unsorted", i, h.last_active, h.string())
        ;}

        humans_circle.sort("last_active")

        ;for i, h in humans_circle.elements() {
        ;    this.days.i("assign", "sorted", i, h.last_active, h.string())
        ;}


        ;this.days.d("assign", 0, "oustables", oustables)
        
        if (active_human) {

            if (active_human.feeling_scared) {
                ;this.days.d("assign", "ACTIVE AND SCARED", human.short)
                active_human.rune := ""
                active_human := false
                goto active_human_done
            }

            if (active_human.rune and active_human.rune.is_destroyed = false) {
                active_human.in_danger := false
        
            } else {
                ; human will need to be placed somewhere on screen
                vacancy := vacancies.pop()

                if (vacancy) {
                    active_human.rune := vacancy
                    active_human.in_danger := false
                    ;this.days.d("assign", "ACTIVE found VACANCY", active_human.short)
                } else {
                    rune := oustables.pop()

                    if (rune) {
                        ousted := rune.human
                        ousted.rune := ""
                        active_human.rune := rune
                        active_human.in_danger := false
                        ;this.days.d("assign", "ACTIVE OUSTED with rune", ousted.short)
                    } else {
                        ; newly active window has to go somewhere
                        rune := runes[1]
                        ousted := rune.human
                        ousted.rune := ""
                        active_human.rune := rune
                        active_human.in_danger := false
                        ;this.days.d("assign", "ACTIVE OUSTED first entry", ousted.short)
                        
                    }
                }
            }
        }
active_human_done:
        for i, human in newcomers {
process_new_human:

            if (human.feeling_scared) {
                ;this.days.d("assign", "+++", "new human feels scared", human.short)
                human.rune := ""
                human.in_danger := true
                continue
            }
            
            if (human.rune) {
                if (human.rune.is_destroyed) {
                    ;this.days.d("assign", "+++", "human.rune.is_destroyed", human.short)
                    
                    human.rune := ""
                    goto process_new_human
                } 
                continue
            } 

            vacancy := vacancies.pop()

            if (vacancy) {
                ;this.days.d("assign", "+++", "NEW HUMAN found VACANCY", human.short)
                
                human.rune := vacancy
                human.in_danger := false
            } else {
                rune := oustables.pop()

                if (rune) {
                    
                    ousted := rune.human
                    ousted.rune := ""
                    human.rune := rune
                    human.in_danger := false
                    ;this.days.d("assign", "+++", "NEW HUMAN OUSTED", ousted.short)
                    
                } 
            }
        
        }

        for i, human in humans {
process_human:
            ;this.days.d("assign", "+++", "process_human", human.short)

            if (human.feeling_scared) {
                if (human.rune) {
                    ;this.days.d("assign", "===", "human feels scared", human.short)
                    
                    human.rune := ""
                    human.in_danger := true
                    
                    continue
                }
            }

            if (human.rune) {
                if (human.rune.is_destroyed) {
                    ;this.days.d("assign", "===", "human.rune.is_destroyed", human.short)
                    
                    human.rune := ""
                    goto process_human
                } 
                continue
            } 
        
            vacancy := vacancies.pop()

            if (vacancy) {
                ;this.days.d("assign", "+++", "HUMAN found VACANCY", human.short)                
                human.rune := vacancy
                human.in_danger := false
            } else {
                ;this.days.d("assign", "+++", "HUMAN should HIDE", human.short)
                
                human.rune := ""
                human.in_danger := true
            }
        }

    }

    msgbox(title, string) {
        ScrollBox(string, "b1 f{s12 cBlack, Consolas}", title) 
    }

}
