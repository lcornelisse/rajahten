class Days {

    static debugging := on

    _subject := ""
    _chronicles := ""

    _d := "--- "
    _i := "*** "
    _w := "??? "
    _e := "!!! "

    __new(subject) {
        this._subject := subject
        this.i("Days started")
    }

    d(args*) {
        SetFormat, IntegerFast, Hex
        this._log(this._d, args)
        SetFormat, IntegerFast, Dec
    }

    i(args*) {
        this._log(this._i, args)
    }

    w(args*) {
        this._log(this._w, args)
    }

    e(args*) {
        this._log(this._e, args)
    }

    forget() {
        this._chronicles := ""
        this.i("Days restarted")
    }

    msgbox() {
        ScrollBox(this._chronicles, "b1 f{s12 cBlack, Consolas}", this._subject) 
    }

    notepad() {
        DetectHiddenText, On
        SetTitleMatchMode 2
        SetTitleMatchMode Slow

        Run, Notepad.exe, C:\, max 
        WinWaitActive, Notepad 
        SendInput, % this._chronicles
    }

    publish() {
        this.msgbox()
    }

    _expand(args) {
        X := " :: "
        s := this._subject
        
        for i, o in args {
            s .= X . o
        } 

        return s
    }

    _log(prefix, args) {
        s := prefix . this._expand(args) . "`n"
        this._chronicles .= s
    }


}