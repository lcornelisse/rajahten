
class RuneOfControl {
    static pick := -1
    static fixed_height := 30

    name := ""
    ordinal := 0

    pos := new Dimensions(0,0,0,0)
    human_pos := new Dimensions(0,0,0,0)
    human_pos_rune_hidden := new Dimensions(0,0,0,0)
    adjustment := new Dimensions(0,0,0,0)

    trans := 0

    human := ""
    
    is_destroyed := false

    stone_set := ""

    __new(hidden) {
       
        gui, New, +HwndName, % "Rune of Control"
        this.name := name

        this.hidden := hidden
        ;RajAhten().days.d("new RuneOfControl", 1, name)
        Gui, %name%:default

        Gui, Margin, 0, 0
        Gui, -Caption -DPIScale +ToolWindow
        Gui, Show

        Gui, Color, EEAA99
        Gui +LastFound  ; Make the GUI window the last found window for use by the line below.
        WinSet, TransColor, EEAA99

        this.stone_set := new StoneSet(this)

        ;this.inferno1 := new Stone(new InfernoMagic(this, 1))
        ;this.inferno1.create_image("C:\p\RajAhten\assets\inferno_1.png", 30, 30)

        ;this.inferno2 := new Stone(new InfernoMagic(this, 2))
        ;this.inferno2.create_image("C:\p\RajAhten\assets\inferno_2.png", 30, 30)

        ;this.inferno3 := new Stone(new InfernoMagic(this, 3))
        ;this.inferno3.create_image("C:\p\RajAhten\assets\inferno_3.png", 30, 30)

        ;this.inferno4 := new Stone(new InfernoMagic(this, 4))
        ;this.inferno4.create_image("C:\p\RajAhten\assets\inferno_4.png", 29, 30)


        this.locus := new Stone(new LocusMagic(this))
        this.locus.create_image("C:\p\RajAhten\assets\octo.png", 28, 28)
        ;this.locus.create_image("C:\p\RajAhten\assets\eyes2.png", 48, 30)

        ;this.invincible := new Stone(new InvincibleMagic(this))
        ;this.invincible.create_image("C:\p\RajAhten\assets\bang.png", 30, 30)

        
        this.star := new Stone(new HideCaptionMagic(this))
        this.star.create_image("C:\p\RajAhten\assets\star.png", 28, 27)

        this.spiral := new Stone(new HidingMagic(this))
        this.spiral.create_image("C:\p\RajAhten\assets\spiral.png", 24, 28)

        this.margin := new Stone(new MarginsMagic(this))
        this.margin.create_image("C:\p\RajAhten\assets\tri2_25.png", 35, 25)
        
        this.bias := new Stone(new BiasMagic(this))
        this.bias.create_image("C:\p\RajAhten\assets\circle5_25_25.png", 25, 25)

        if (hidden = false) {
            Gui, Show, NA
        } else {
            Gui, Hide
        }

    }

    __delete() {
        this.destroy()
    }

    destroy() {
        if (this.is_destroyed = false) {
            this.is_destroyed := true
            ;RajAhten().days.d("Destroying rune", this.name)
            this.stone_set.destroy()

            this_rune := this.name 

            try {
                this.set_default_gui()
                Gui, Destroy
            }

            this.name := ""
            ordinal := ""
            pos := ""
        }
    }

    string() {
        return "%%% " . this.name . " %%%" 
    }

    set_default_gui() {
        name := this.name
        ;RajAhten().days.d("set_default_gui", "SetFormat, IntegerFast, Hex", name)
        Gui, %name%:default
    }

    show() {
        ;if (this.hidden) {
            this.set_default_gui()
            Gui, Show, NA
            this.hidden := false
        ;}
    }

    hide() {
        if (this.hidden = false) {
            this.hidden := true
            this.position_quicker()
        }
    }

    hide_but_dont_resize_anything() {
        if (this.hidden = false) {
            this.set_default_gui()
            Gui, Hide
            this.hidden := true
        }
    }

    get_adjusted_human_pos() {
        
        if (this.hidden) {
            pos := this.human_pos_rune_hidden.clone()
        } else {
            pos := this.human_pos.clone()
        }

        this.adjustment := human.adjustment
        pos.x += this.adjustment.x
        pos.y += this.adjustment.y
        pos.w += this.adjustment.w
        pos.h += this.adjustment.h
        
        return pos
    }

    position() {
        pos := this.pos

        SetFormat, IntegerFast, Dec
        
        shrinkage := 10
        X := pos.X + shrinkage
        Y := pos.Y
        
        W := pos.W - (2 * shrinkage)
        H := pos.H
        
        ;RajAhten().days.d("positioning rune", 1, this_rune, X, Y, W, H)
        this.set_default_gui()      
        Gui, Margin, 0, 0
        Gui, -Caption -DPIScale
        
        this.inscribe()
        this.stone_set.position_stones()
        this.position_icons(W)

        ; this causes a flicker when hidden. is needed though. 
        Gui, Show, NA X%X% Y%Y% W%W% H%H%

        if (this.hidden) {
            Gui, Hide
        } else {
            Gui, Show, NA X%X% Y%Y% W%W% H%H%
        }

    }

    position_quicker() {
        pos := this.pos

        SetFormat, IntegerFast, Dec
        shrinkage := 10
        X := pos.X + shrinkage
        Y := pos.Y
        W := pos.W - (2 * shrinkage)
        H := pos.H

        ;RajAhten().days.d("positioning rune", 1, this_rune, X, Y, W, H)
        this.set_default_gui()      
        
        this.stone_set.position_stones()
        this.position_icons(w)
        
        if (this.hidden) {
            Gui, Hide
        } else {
            Gui, Show, X%X% Y%Y% W%W% H%H%
        }
    }

    position_icons(w) {

        ; far left
        this.locus.pos.x := 5
        this.locus.pos.y := 0
        this.locus.position()

        ; far right
        ZX := w - this.bias.pos.w
        this.bias.pos.x := ZX
        this.bias.position()

        ; to the left of each on the far right
        far_right := [this.margin, this.star, this.spiral]
        ;, this.invincible, this.inferno4, this.inferno3, this.inferno2, this.inferno1]

        for i, o in far_right {
            ;RajAhten().days.d("position_icons", i, o.magic.derp)
            o.pos.w := 35
            ZX := ZX - (2 * o.pos.w)  
            o.pos.x := ZX
            o.position()
        }
    }

    take_human(human) {
        this_name := this.name

        human.update()
        if (human.is_dead) {
            ;RajAhten().days.d("rune", this.name, "take_human", 0, human.short, "human is dead.")
            return
        }

        ;RajAhten().days.d("rune", this.name, "take_human", 1, human.short)
        this.human := human
        
        ;RajAhten().days.d("rune", this.name, "take_human", 2, human.short)
        human.rune := this

        ; manipulation!
        human.in_danger := false
    }

    inscribe() {
        ;RajAhten().days.d("--------------------------------------")
        ;RajAhten().days.d("---( Inscribing Rune )----------------")
        this.stone_set.clear()
        ;RajAhten().days.d("Cleared.")

        for i, human in EarthKing().army {
            cloaked := EarthWarden().isWindowCloaked(human.name)

            if (cloaked = 0 and human.rune != this) {
                this.stone_set.insert_human(human)                
            }
        }
        ;RajAhten().days.d("Inscribed rune.")
        ;RajAhten().days.d("--------------------------------------")
        ;RajAhten().days.d("--------------------------------------")
    }

    reinscribe_if_needed() {
        if (this.stone_set.has_human(this.human)) {
            this.position()
        }
    }

    reinscribe() {
        this.position()
    }
}

