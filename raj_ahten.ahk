RajAhten() {
    return RajAhten.instance
}

class RajAhten {
    static instance := new RajAhten()

    days := new Days("Raj Ahten")

    seals := {}
    seal := ""
    graak := ""
    land_name := ""

    hidden_ww := new WaterWizard(false, true)
    hidden := this.hidden_ww.look()

	__new() {
        this.days.i("Look at meeeeeeee!", this.hidden)
    }

    __delete() {
    }

    create_seal(num, land_name) {
        ;this.days.i("making seal of inferno")
        seal := new SealOfInferno(this.hidden)
        this.seal := seal
        this.seals[land_name] := seal
        ; this is slow
        seal.number_of_runes(num) 
        seal.spoiled := true
    }

    update_seal() {
        seal := this.seal

        any_dead := EarthKing().clear_dead_humans()
        newcomers := EarthKing().choose()

        if (any_dead or newcomers.length() > 0) {
            seal.spoiled := true
        }

        ;this.days.i("arranging seal of inferno")
        seal.arrange()

        EarthKing().your_move()
        ;this.days.i("The Wolf Lord admires his handiwork")
    }

    seal_of_inferno(num) {
        seal := this.seal
        if (seal.type != SealOfInferno) {
            seal.destroy()
            seal := new SealOfInferno(this.hidden)
            this.seal := seal
            this.seals[this.land_name] := seal
        }
        seal.number_of_runes(num)
        seal.spoiled := true
        ;this.show()
    }

    seal_of_poisonwood() {
        seal := this.seal
        if (seal.type != SealOfPoisonwood) {
            seal.destroy()
            seal := new SealOfPoisonwood(this.hidden)
            this.seal := seal
            this.seals[this.land_name] := seal            
        }
        seal.spoiled := true
        ;this.show()
    }

    toggle_hidden() {
        this.hidden := this.hidden_ww.poke()
        ;this.days.d("toggle_hidden", this.hidden)
        this.render()
    }

    toggle_margins() {
        this.seal.toggle_margins()
        this.seal.arrange_quicker()
    }

    render() {
        this.hidden := this.hidden_ww.look()
        ;this.days.d("render", this.hidden)
        if (this.hidden) {
            for land_name, seal in this.seals {
                seal.hide()
            }
        } else {
            for land_name, seal in this.seals { 
                seal.show()
                if (land_name = this.land_name) {
                    ;this.days.d("render", land_name, this.land_name)           
                    seal.show_runes()
                }
            }
        }
    }

    hide() {
        if (this.hidden = false) {
            this.hidden_ww.poke()
        }
        this.render()
    }

    show() {
        if (this.hidden) {
            this.hidden_ww.poke()
        }
        this.render()
    }

    land_has_changed(graak) {

        ;this.days.i("Flameweavers report that the land has changed.", "graak", graak)

        this.graak := graak

        land := EarthKing().land_has_changed(graak)
        land_name := land.name
        this.land_name := land_name

        ;this.days.i("Land name: ", land_name)

        found_seal := this.seals[land_name]

        if (found_seal = "") {
            this.create_seal(3, land_name)
        } else {
            this.seal := found_seal
        }

        for did, seal in this.seals {
            if (did = land_name) {
                seal.show_runes()
                ;if (GetKeyState("Capslock", "T")) {
                ;    seal.show_runes()
                ;} else {
                ;    seal.hide_runes()
                ;}
            } else {
                ;seal.hide_runes()
                seal.hide_runes_but_dont_resize_anything()
            }
        }

        ;this.update_seal()
    }

    tick(graak) {

        seal := this.seal

        if (seal = "") {
            msgbox, wtf
        }

        ;if (GetKeyState("Capslock", "T")) {
        ;    seal.show_runes()
        ;} else {
        ;    seal.hide_runes()
        ;}
    
        this.update_seal()
    
    }

}
