;;
;; O Light Of The Universe
;;
#SingleInstance

SetBatchLines -1

;;
;; grace from windows10DesktopManager
;;
globalDesktopManager := new JPGIncDesktopManagerClass()
globalDesktopManager.setGoToDesktop("Capslock")
    .setMoveWindowToDesktop("+#")
    .afterGoToDesktop("turnCapslockOff")
    .afterMoveWindowToDesktop("turnCapslockOff")
    .setGoToNextDesktop("Capslock & w")
    .setGoToPreviousDesktop("Capslock & q")
    .setMoveWindowToNextDesktop("Capslock & s")
    .setMoveWindowToPreviousDesktop("Capslock & a")
	;~ .setCloseDesktop("Capslock & x")
	;~ .setNewDesktop("Capslock & n")

#Include dedicates_keep\windows10DesktopManager\desktopManager.ahk
#Include dedicates_keep\windows10DesktopManager\desktopChanger.ahk
#Include dedicates_keep\windows10DesktopManager\windowMover.ahk
#Include dedicates_keep\windows10DesktopManager\desktopMapper.ahk
#include dedicates_keep\windows10DesktopManager\virtualDesktopManager.ahk
#Include dedicates_keep\windows10DesktopManager\monitorMapper.ahk
#Include dedicates_keep\windows10DesktopManager\hotkeyManager.ahk
#Include dedicates_keep\windows10DesktopManager\commonFunctions.ahk
debugger(message) 
{
	;ToolTip, % message
	;sleep 100
	return
}

;;
;; sight from WinGetPosEx
;;
#Include dedicates_keep\WinGetPosEx.ahk 

;;
;; grace from Class_ImageButton
;;
#Include dedicates_keep\Class_ImageButton\Sources\Class_ImageButton.ahk

;;
;; sight from FanaticGuru
;;
#Include dedicates_keep\ScrollBox.ahk

#Include house_of_understanding.ahk
#Include runelore.ahk
#Include days.ahk
#Include gaborn.ahk
#Include the_wizard_binnesman.ahk
#Include flameweavers.ahk
#Include raj_ahten.ahk
#include magic.ahk
#include stones.ahk
#include runes.ahk
#include the_green_woman.ahk
#include guildmaster_hollicks.ahk
#include averan.ahk

#1:: RajAhten().seal_of_inferno(1)
#2:: RajAhten().seal_of_inferno(2)
#3:: RajAhten().seal_of_inferno(3)
#4:: RajAhten().seal_of_inferno(4)
#5:: RajAhten().seal_of_poisonwood()

#r:: RajAhten().toggle_hidden()
#m:: RajAhten().toggle_margins()

#w:: RajAhten().seal.whorl()
#+w:: RajAhten().seal.anti_whorl()
#^w:: RajAhten().seal.whorl_inactive()
#^+w:: RajAhten().seal.anti_whorl_inactive()


#t:: RajAhten().days.publish()
#+t:: EarthKing().days.publish()
    
#y:: EarthWarden().days.publish()

#+u:: EarthWarden().what_do_you_see()

#+i:: Wizardborn().prepare_dummies()
#+o:: Wizardborn().test_sort_humans()


#+r:: Reload

TimeIsMarching() {
    ;RajAhten().days.d("Flameweavers are watching")
    SetWinDelay, -1
    
    ;Spring().hunt()

    EarthKing().watch_for_changes(Flameweavers().watch_for_changes())

    SetTimer, TimeIsMarching, -300
}

#n::
    TimeIsMarching()
    return

#+f::
    Flameweavers().stop_watching()
    return
