
Wizardborn() {
    return Averan.instance
}
Averan() {
    return Averan.instance
}
class Averan {
	static instance := new Averan()
    days := new Days("Averan")
    
    dummies := {}

	__new() {
        this.days.i("The young Wizardborn misses flying, but gains something more sublime, deep in the earth, testing theories.")
    }
    
    __delete() {
    }

    prepare_dummies() {
        this.dummies := new Circle()
        n := 300000
        i := 0

        while (i < n) {
            Random, x, 1, n
            dummy := new DummyHuman()
            dummy.last_active := x
            this.dummies.push(dummy)
            i++
        }

        s := ""

        ;s := s . "`nUnsorted dummies:`n   "
        ;for i, h in this.dummies.elements() {
        ;    s := s . ", " . h.last_active
        ;    if (mod(i,30) = 0) {
        ;        s := s . "`n   "
        ;    }
        ;}
        s := "done"
        this.msgbox("Wizardborn :: prepare_dummies", s)

    }

    test_sort_humans() {
        dummies := this.dummies

        t_start := A_TickCount
        dummies.sort("last_active")
        ;dummies.gnomesort("last_active")
        t_end := A_TickCount
        
        s := s . "`n`nSorted dummies:`n   "
        ;for i, h in dummies.elements() {
        ;    s := s . ", " . h.last_active
        ;    if (mod(i,30) == 0) {
        ;        s := s . "`n   "
        ;    }
        ;}

        took := (t_end - t_start) / 1000.0
        n := dummies.length()
        s := s . "`n`n" . n . " items took: " . took . " seconds."

        s := s . "`n         gnomes: " . dummies.gnomes
        s := s . "`n         merges: " . dummies.merges
        s := s . "`n   merge_copies: " . dummies.merge_copies
        s := s . "`nmerge_depth_max: " . dummies.merge_depth_max
        

        this.msgbox("Wizardborn :: test_sort_humans", s)

    }

    msgbox(title, string) {
        ScrollBox(string, "b1 f{s12 cBlack, Consolas}", title) 
    }

}

class DummyHuman {

    last_active := 0
}